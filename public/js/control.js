// check radio button
var checkRadio = function() {
  var gRadio = document.getElementsByClassName('form-check-input');
  for (var i = 0; i < gRadio.length; i++) {
    if (gRadio[i].checked) {
      return gRadio[i].value;
    }
  }
};
var radioValue = checkRadio();
// End check radio button and return to radio value
//define value
var departing = $('#departing').val();
var adults = $('#adults').val();
var returning = $('#returning').val();
var cabin = $('#cabin').val();
var flyingTo = $('#flyingTo').val();
var flyingFrom = $('#flyingFrom').val();
var maxIndex = 0;
var filterData = [];
//End define value
var getJsonData = new Promise(function(resolve, reject) {
  $.getJSON('/static/data/Flight.json', function(data) {
    resolve(data);
  });
});

function createTableAndDelete(id, value) {
  $("#" + id).html('<tr><td style="font-size:125%"><a href="#">' + value.price + '</a></td><td>' + value.airline + '</td><td>' +
    value.takeOff + " " + value.time[0] + '</td><td>' + value.landing + " " + value.time[1] +
    '</td><td>' + value.stop +
    '<a href="#">details</i></a> <a href="#"><i class="fas fa-envelope"></i></a> <a href="#"><i class="fas fa-print"></i></a> <a href="#"><i class="far fa-heart"></i></a> <a href="#"><i class="fas fa-trash-alt"></i></a></td></tr>');
  $("#demo").append(' hello' + '<br>');
}

function createTable(id, value) {
  $("#" + id).append('<tr><td style="font-size:125%"><a href="#">' + value.price + '</a></td><td>' + value.airline + '</td><td>' +
    value.takeOff + " " + value.time[0] + '</td><td>' + value.landing + " " + value.time[1] +
    '</td><td>' + value.stop +
    '<a href="#">details</i></a> <a href="#"><i class="fas fa-envelope"></i></a> <a href="#"><i class="fas fa-print"></i></a> <a href="#"><i class="far fa-heart"></i></a> <a href="#"><i class="fas fa-trash-alt"></i></a></td></tr>');
  $("#demo").append(' hello' + '<br>');
}

function createCardAndDelete(id, value) {
  $("#" + id).html('<div class="card-group"><div class="card bg-white col-2">' +
    '<div class="card-body text-center">' +
    '<p class="card-text">' +
    '<span>' +
    '<a href="#">' +
    '<h2><span>$<u>' + value.price + '<br><h5>select</h5></u></span></h2>' +
    '</a>' +
    '</span>' +
    '</p>' +
    '</div>' +
    '</div>' +
    '<div class="card bg-white col-10">' +
    '<div class="card-body text-left">' +
    '<table class="table table-borderless ">' +
    '<thead id="table_card">' +
    '<tr>' +
    '<th style="width: 30%" class="text-center"><img src="static/images/' + value.image + '" class="img-rounded" alt="Cinque Terre" width="50" height="50"><br>' + value.airline + '</th>' +
    '<th <th style="width: 25%">' + value.takeOff + " " + value.time[0] + '</th>' +
    '<th style="width: 25%">' + value.landing + " " + value.time[1] + '</th>' +
    '<th>' + value.stop + '</th>' +
    '</tr>' +
    '</thead>' +
    '</table>' +
    '</div>' +
    '</div></div>' +
    '<br>'
  );
}

function createCard(id, value) {
  $("#" + id).append('<div class="card-group"><div class="card bg-white col-2">' +
    '<div class="card-body text-center">' +
    '<p class="card-text">' +
    '<span>' +
    '<a href="#">' +
    '<h2><span>$<u>' + value.price + '<br><h5>select</h5></u></span></h2>' +
    '</a>' +
    '</span>' +
    '</p>' +
    '</div>' +
    '</div>' +
    '<div class="card bg-white col-10">' +
    '<div class="card-body text-left">' +
    '<table class="table table-borderless ">' +
    '<thead id="table_card">' +
    '<tr>' +
    '<th style="width: 30%" class="text-center"><img src="static/images/' + value.image + '" class="img-rounded" alt="Cinque Terre" width="50" height="50"><br>' + value.airline + '</th>' +
    '<th <th style="width: 25%">' + value.takeOff + " " + value.time[0] + '</th>' +
    '<th style="width: 25%">' + value.landing + " " + value.time[1] + '</th>' +
    '<th>' + value.stop + '</th>' +
    '</tr>' +
    '</thead>' +
    '</table>' +
    '</div>' +
    '</div></div>' +
    '<br>'
  );
}
var requestJsonData = function() {
  getJsonData.then(function(fullfiled) {
    var checkIndex = 0;
    var limitShowOnTable = 3;
    var searchNotFound = 0;
    for (var i = 0; i < fullfiled.length; i++) {
      if (fullfiled[i].takeOff == departing && fullfiled[i].landing == returning) {
        filterData.push(fullfiled[i]);
        maxIndex++;
        checkIndex++;
        if (checkIndex <= limitShowOnTable) {
          createTable("table_body", fullfiled[i]);
          createCard("table_card", fullfiled[i]);
        }
      }
    }
    for (var num = 1; num <= Math.ceil(maxIndex / limitShowOnTable); num++) {
      $('.pagination').append('<li class="page-item"><a class="page-link" href="javascript: onClick=showTable(' + num + ','+limitShowOnTable+')">' + num + '</a></li>');
    }
    if (maxIndex == searchNotFound)
      alert("Airline not found");
    else {
      $('p.showIndex').append("<h4><b>1 of " + maxIndex + " flights show " + radioValue + "<b></h4>");
    }
  }).catch(function(error) {
    alert(error.message);
  });
};

function showTable(num,limitShowOnTable) {
  var start = (num - 1) * limitShowOnTable;
  var checkIndex = 0;
  for (var i = start; i < filterData.length; i++) {
    checkIndex++;
    if (checkIndex <= limitShowOnTable) {
      if (i == start) {
        createTableAndDelete("table_body", filterData[i]);
        createCardAndDelete("table_card", filterData[i]);
      } else {
        createTable("table_body", filterData[i]);
        createCard("table_card", filterData[i]);
      }
    } else {
      break;
    }
  }
  var indexStartOntable = (((num - 1) * limitShowOnTable) + 1);
  $('p.showIndex').html("<h4><b>" + indexStartOntable + " of " + maxIndex + " flights show " + radioValue + "</b></h4>");
}
requestJsonData();
