var express = require('express');
// var bodyParser = require('body-parser')

var path = require('path');
var app = express();
//var publicDir - path.join(__dirname,'/public')
//app.use(express.static(publicDir))
app.use('/static',express.static(path.join(__dirname,'public')));
app.get('/',function(req,res)
{//use absolute path to html file
  res.sendFile(path.join(__dirname,'searchFlight.html'));
}
);
app.get('/displayFlights.html',function(req,res)
{//use absolute path to html file
  res.sendFile(path.join(__dirname,'displayFlights.html'));
}
);
app.listen(3000,function(){
  console.log('Server Start');
}
);
